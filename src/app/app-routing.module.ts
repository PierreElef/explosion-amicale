import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'mines',
    loadChildren: () => import('./pages/mines/mines.module').then( m => m.MinesPageModule)
  },
  {
    path: 'crea',
    loadChildren: () => import('./pages/crea/crea.module').then( m => m.CreaPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'game',
    loadChildren: () => import('./pages/game/game.module').then( m => m.GamePageModule)
  },
  {
    path: 'games-list',
    loadChildren: () => import('./pages/games-list/games-list.module').then( m => m.GamesListPageModule)
  },
  {
    path: 'wait',
    loadChildren: () => import('./pages/wait/wait.module').then( m => m.WaitPageModule)
  },
  {
    path: '',
    redirectTo: 'crea',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
