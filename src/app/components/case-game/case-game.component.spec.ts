import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CaseGameComponent } from './case-game.component';

describe('CaseGameComponent', () => {
  let component: CaseGameComponent;
  let fixture: ComponentFixture<CaseGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseGameComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CaseGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
