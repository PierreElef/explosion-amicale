import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-case-game',
  templateUrl: './case-game.component.html',
  styleUrls: ['./case-game.component.scss'],
})
export class CaseGameComponent implements OnInit {

  @Input() case: number;
  @Input() status: boolean;

  // Give info to parent : grid-game

  @Output() info = new EventEmitter();

  isGood = false;
  isNotGood = false;
  isClicked = false;

  constructor() { }

  ngOnInit() { }

  // When player click on case, change case's color

  onClick() {

    // Replace switch case by request on bdd for check if there is mine here

    if (!this.isClicked) {

      this.isClicked = true;

      if (this.status) {
        this.isGood = true;
      } else {
        this.isNotGood = true;
      }

      // Give info to parent : grid-game

      this.info.emit(
        {
          idCase: this.case,
          click: this.isClicked
        }
      );

    }

  }

}
