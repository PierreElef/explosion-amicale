import { Component, OnInit, Input } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { PlayerService } from 'src/app/services/player/player.service';

@Component({
  selector: 'app-case-me',
  templateUrl: './case-me.component.html',
  styleUrls: ['./case-me.component.scss'],
})
export class CaseMeComponent implements OnInit {


  @Input() case: number;
  @Input() status: boolean;
  @Input() adID: number;
  isGood = false;
  isNotGood = false;

  constructor(private socket: Socket, private playerService: PlayerService) { }

  ngOnInit() {

    // Event Socket player turn change : check if the box is touched or not
    this.socket.fromEvent('changeTurnFromBack').subscribe(data => {
      let coupsAd = [];
      this.playerService.getPlayerById(this.adID).subscribe(resPlayer => {
        coupsAd = resPlayer.coups.substring(1, resPlayer.coups.length - 1).split(',');
        if (coupsAd.includes(this.case.toString())) {
          if (this.status) {
            this.isGood = true;
          } else {
            this.isNotGood = true;
          }
        }
      });
    });

  }
}
