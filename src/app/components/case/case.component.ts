import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-case',
  templateUrl: './case.component.html',
  styleUrls: ['./case.component.scss'],
})
export class CaseComponent implements OnInit {

  @Input() caseId: string;
  @Input() fiveCases: boolean;
  @Output() caseValided = new EventEmitter<string>();
  className = 'case text-center';

  constructor() { }

  ngOnInit() { }

  onValidClick() {
    if (this.className === 'case text-center') {
      if (!this.fiveCases) {
        this.className = 'case text-center valid';
      }
    } else {
      this.className = 'case text-center';
    }
    this.caseValided.emit(this.caseId);
  }

}
