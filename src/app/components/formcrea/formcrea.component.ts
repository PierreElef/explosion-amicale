import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-formcrea',
  templateUrl: './formcrea.component.html',
  styleUrls: ['./formcrea.component.scss'],
})
export class FormcreaComponent implements OnInit {

  // Give pseudo to parent : creaPage

  @Output() info = new EventEmitter();

  // Take boolean isOk from parent : creaPage

  @Input() isOk: boolean;

  pseudo: string;

  pseudoIncorrect = false;

  constructor() { }

  ngOnInit() {

    /*

      If isOk == true, request on bdd for find an other player, if no one => wait 10 sec and do the same

      When an other player was found, redirect to mines page and start game

    */

  }

  // Emit event with player's pseudo

  submit() {
    if (this.pseudo !== undefined) {
      this.pseudoIncorrect = false;
      this.info.emit(this.pseudo);
    } else {
      this.pseudoIncorrect = true;
    }
  }

}
