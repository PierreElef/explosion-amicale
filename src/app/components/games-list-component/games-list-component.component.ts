import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Game } from 'src/app/models/game';
import { Player } from 'src/app/models/player';
import { PlayerService } from 'src/app/services/player/player.service';
import { Socket } from 'ngx-socket-io';
import { Router } from '@angular/router';

@Component({
  selector: 'app-games-list-component',
  templateUrl: './games-list-component.component.html',
  styleUrls: ['./games-list-component.component.scss'],
})
export class GamesListComponentComponent implements OnInit {

  @Input() game: Game;

  @Input() refresh: void;

  @Output() info = new EventEmitter();

  player: Player;

  constructor(private socket: Socket, private playerService: PlayerService, private router: Router) { }

  ngOnInit() {
  }

  addPlayerToGame() {

    // Récupération player en localStorage
    this.player = JSON.parse(localStorage.getItem('user'));

    // MAJ player par rapport BDD
    this.playerService.getPlayerById(this.player.id).subscribe((resPlayer) => {
      // Add play in game
      resPlayer.game_id = this.game.id;
      // Update player
      this.playerService.setPlayerById(this.player.id, resPlayer).subscribe((res) => {
        localStorage.setItem('user', JSON.stringify(res));
        // Connexion to socket into server
        this.socket.connect();
        // Emit event startGame to server node js
        this.socket.emit('startGame', res.game_id);
        // Redirect to wait page
        this.router.navigate(['/wait']);

      });

    });

  }

}
