import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GridGameComponent } from './grid-game.component';

describe('GridGameComponent', () => {
  let component: GridGameComponent;
  let fixture: ComponentFixture<GridGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridGameComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GridGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
