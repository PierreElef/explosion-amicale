import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { Player } from 'src/app/models/player';
import { PlayerService } from 'src/app/services/player/player.service';

@Component({
  selector: 'app-grid-game',
  templateUrl: './grid-game.component.html',
  styleUrls: ['./grid-game.component.scss'],
})
export class GridGameComponent implements OnInit {

  // Give info to parent : grid page

  casesVides = [];
  player: Player;

  @Output() info = new EventEmitter();

  @Input() playerID: number;

  constructor(private playerService: PlayerService) { }

  ngOnInit() {

    // Generation of the opponent's grid
    this.playerService.getPlayerById(this.playerID).subscribe((resPlayer) => {
      this.player = resPlayer;
      const casesValid = this.player.mines.substring(1, this.player.mines.length - 1).split(',');
      let statusCase: boolean;
      for (let i = 0; i < 25; i++) {
        if (casesValid.includes(i.toString())) {
          statusCase = true;
        } else {
          statusCase = false;
        }
        this.casesVides.push(
          {
            id: i,
            status: statusCase
          }
        );
      }
    });
  }

  // Take info from child : case-game and Give info to parent : grid page
  getInfo(info) {
    this.info.emit(info);
  }

}
