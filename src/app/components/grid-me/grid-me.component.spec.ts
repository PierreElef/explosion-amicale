import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GridMeComponent } from './grid-me.component';

describe('GridMeComponent', () => {
  let component: GridMeComponent;
  let fixture: ComponentFixture<GridMeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridMeComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GridMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
