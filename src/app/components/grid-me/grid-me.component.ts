import { Component, OnInit, Input } from '@angular/core';
import { Player } from 'src/app/models/player';
import { PlayerService } from 'src/app/services/player/player.service';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-grid-me',
  templateUrl: './grid-me.component.html',
  styleUrls: ['./grid-me.component.scss'],
})
export class GridMeComponent implements OnInit {

  casesVides = [];
  @Input() playerID: number;
  @Input() adID: number;
  player: Player;

  constructor(private playerService: PlayerService) { }

  ngOnInit() {
    // Generation of the player's grid
    this.playerService.getPlayerById(this.playerID).subscribe((resPlayer) => {
      this.player = resPlayer;
      const casesValid = this.player.mines.substring(1, this.player.mines.length - 1).split(',');
      let statusCase: boolean;
      for (let i = 0; i < 25; i++) {
        if (casesValid.includes(i.toString())) {
          statusCase = true;
        } else {
          statusCase = false;
        }
        this.casesVides.push(
          {
            id: i,
            status: statusCase
          }
        );
      }
    });
  }


}
