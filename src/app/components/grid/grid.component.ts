import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss'],
})
export class GridComponent implements OnInit {

  caseIds = [];
  casesValid = [];
  @Input() fiveCases: boolean;
  @Output() casesValided = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    this.casesValid = [];
    // Generate grid for case choise
    for (let i = 0; i < 25; i++) {
      this.caseIds.push(i);
    }
  }

  onValidCase(res) {
    let check = true;
    const newArray = [];
    this.casesValid.forEach(caseValid => {
      if (caseValid !== res) {
        newArray.push(caseValid);
      } else {
        check = false;
      }
    });
    if (check) {
      newArray.push(res);
    }
    if (newArray.length <= 5) {
      this.casesValid = newArray;
      this.casesValided.emit(JSON.stringify(this.casesValid));
    }
  }

}
