export class Player {
    id: number;
    pseudo: string;
    mines: string;
    coups: string;
    // tslint:disable-next-line:variable-name
    game_id: number;
}
