import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreaPageRoutingModule } from './crea-routing.module';

import { CreaPage } from './crea.page';
import { FormcreaComponent } from 'src/app/components/formcrea/formcrea.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreaPageRoutingModule
  ],
  declarations: [CreaPage, FormcreaComponent]
})
export class CreaPageModule {}
