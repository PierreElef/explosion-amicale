import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../../services/player/player.service';
import { HttpClient } from '@angular/common/http';
import { Player } from 'src/app/models/player';
import { Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-crea',
  templateUrl: './crea.page.html',
  styleUrls: ['./crea.page.scss'],
})
export class CreaPage implements OnInit {

  pseudo: string;
  isOk = false;

  constructor(private socket: Socket, private playerService: PlayerService, private http: HttpClient, private router: Router) { }

  ngOnInit() {
    localStorage.clear();
  }


  // Give pseudo to child : formcrea

  getPseudo(pseudo: string){
    this.pseudo = pseudo;
    this.isOk = true;
    const player = new Player();
    player.pseudo = pseudo;
    player.mines = '';
    player.coups = '';
    this.playerService.createPlayer(player).subscribe((res) => {

      // Set user in localstorage

      localStorage.setItem('user', JSON.stringify(res));

    });
    this.router.navigate(['/mines']);
  }

}
