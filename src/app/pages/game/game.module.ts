import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GamePageRoutingModule } from './game-routing.module';

import { GamePage } from './game.page';
import { GridGameComponent } from 'src/app/components/grid-game/grid-game.component';
import { CaseGameComponent } from 'src/app/components/case-game/case-game.component';
import { GridMeComponent } from 'src/app/components/grid-me/grid-me.component';
import { CaseMeComponent } from 'src/app/components/case-me/case-me.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GamePageRoutingModule
  ],
  declarations: [GamePage, GridGameComponent, CaseGameComponent, GridMeComponent, CaseMeComponent]
})
export class GamePageModule {}
