import { Component, OnDestroy, OnInit } from '@angular/core';
import { Game } from 'src/app/models/game';
import { Player } from 'src/app/models/player';
import { GameService } from 'src/app/services/game/game.service';
import { PlayerService } from 'src/app/services/player/player.service';
import { Socket } from 'ngx-socket-io';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
})
export class GamePage implements OnInit, OnDestroy{

  info: object;
  text: string;
  me: Player;
  adversaire: Player;
  game: Game;
  meID: number;
  adID: number;
  isYourTurn: boolean;
  isFirstTurn = true;
  gameEnd = false;

  constructor(private gameService: GameService, private playerService: PlayerService, private socket: Socket, private router: Router) { }

  ngOnInit() {
    // searching player
    this.playerService.getPlayerById(JSON.parse(localStorage.getItem('user')).id).subscribe((resPlayer) => {
      this.me = resPlayer;
      this.meID = resPlayer.id;
      // searching game
      this.gameService.getGameById(this.me.game_id).subscribe((resGame) => {
        this.game = resGame;
        if (this.isFirstTurn) {
          this.text = 'C\'est à ' + resGame.Players[0].pseudo + ' de jouer !';
          if (resPlayer.id === resGame.Players[0].id) {
            this.isYourTurn = true;
          } else {
            this.isYourTurn = false;
          }
        }
        // searching opponent
        this.adversaire = resGame.Players.filter((player) => player.id !== resPlayer.id)[0];
        this.adID = this.adversaire.id;
      });
    });

    // Event Socket changement de tour
    this.socket.fromEvent('changeTurnFromBack').subscribe((data: any) => {

      const idUserCo = JSON.parse(localStorage.getItem('user')).id;

      // if endGame and it's looser's turn
      if (data.checker && idUserCo !== data.playerId) {
        this.text = 'Défaite !';
        this.isYourTurn = false;
        this.gameEnd = true;
        return;
      }
      // if endGame and it's winner's turn
      else if (data.checker && idUserCo === data.playerId) {
        this.text = 'Victoire !';
        this.isYourTurn = false;
        this.gameEnd = true;
        return;
      }

      // If it's your turn or not

      if (idUserCo !== data.playerId) {
        this.isYourTurn = true;
        this.text = 'C\'est à ' + this.me.pseudo + ' de jouer.';
      } else {
        this.isYourTurn = false;
        this.text = 'C\'est à ' + this.adversaire.pseudo + ' de jouer.';
      }

      console.log('From back with event : ' + data.playerId);

    });

  }

  // Get info from child : grid-game
  // Change text for know who's must play

  getInfo(info) {

    // Get player by id
    this.playerService.getPlayerById(JSON.parse(localStorage.getItem('user')).id).subscribe((resPlayer) => {

      // Get coups
      let coupsBefore;

      // If coups is empty init it else get it

      if (resPlayer.coups === '') {
        coupsBefore = [];
      } else {
        coupsBefore = JSON.parse(resPlayer.coups);
      }

      // Update coups
      coupsBefore.push(info.idCase);
      resPlayer.coups = JSON.stringify(coupsBefore);

      // Update player
      this.playerService.setPlayerById(resPlayer.id, resPlayer).subscribe((res) => {
        // set isFirstTurn to false
        this.isFirstTurn = false;
        // update storage
        const user = JSON.parse(localStorage.getItem('user'));

        // get mines of the other player
        const minesAd = JSON.parse(this.adversaire.mines);

        // Check if end game

        const checker = minesAd.every(m => JSON.parse(res.coups).includes(m));

        // send event to backend
        this.socket.emit('changeTurnFromFront', user.game_id, user.id, checker);

      });

    });

  }

  newGame() {
    this.me.mines = '';
    this.me.coups = '';
    this.me.game_id = null;

    // MAJ BDD and return on page mines
    this.playerService.setPlayerById(this.me.id, this.me).subscribe(res => {
      localStorage.setItem('user', JSON.stringify(res));
      // Redirect to mines
      this.router.navigate(['/mines']);
    });
  }


  ngOnDestroy() {
    this.me.game_id = null;
    this.playerService.setPlayerById(this.me.id, this.me).subscribe();
    this.adversaire.game_id = null;
    this.playerService.setPlayerById(this.adversaire.id, this.adversaire).subscribe();
    this.gameService.deleteGameById(this.game.id);
  }

}
