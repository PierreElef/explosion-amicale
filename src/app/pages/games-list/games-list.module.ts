import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GamesListPageRoutingModule } from './games-list-routing.module';

import { GamesListPage } from './games-list.page';
import { GamesListComponentComponent } from 'src/app/components/games-list-component/games-list-component.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GamesListPageRoutingModule
  ],
  declarations: [GamesListPage, GamesListComponentComponent]
})
export class GamesListPageModule {}
