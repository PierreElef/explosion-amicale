import { Component, OnInit } from '@angular/core';
import { Game } from 'src/app/models/game';
import { Player } from 'src/app/models/player';
import { GameService } from 'src/app/services/game/game.service';
import { PlayerService } from 'src/app/services/player/player.service';
import { Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';

@Component({
	selector: 'app-games-list',
	templateUrl: './games-list.page.html',
	styleUrls: ['./games-list.page.scss']
})
export class GamesListPage implements OnInit {
	games: Game[];
	game: Game;
	player: Player;

	constructor(private gameService: GameService, private playerService: PlayerService, private router: Router, private socket: Socket) { }

	ngOnInit() {

		// On refresh event send, call ngOnInit for refresh
		this.socket.fromEvent('refresh').subscribe((res) => {
			this.ngOnInit();
		});
		this.getGames();

	}

	getGames() {
		// Find all game who have 1 player only
		this.gameService.findGame().subscribe((res) => {
			this.games = res;
		});
	}

	submit() {
		// Récupération player en localStorage
		this.player = JSON.parse(localStorage.getItem('user'));
		// MAJ player par rapport BDD
		this.playerService.getPlayerById(this.player.id).subscribe((resPlayer) => {
			this.gameService.createGame().subscribe((resGame) => {
				this.game = resGame;
				resPlayer.game_id = this.game.id;
				this.playerService.setPlayerById(this.player.id, resPlayer).subscribe((res) => {
					localStorage.setItem('user', JSON.stringify(resPlayer));
					// Connexion to socket into server
					this.socket.connect();
					// Emit event startGame to server node js
					this.socket.emit('createGame', res.game_id);
					// Emit event for refresh that page for all clients
					this.socket.emit('refreshListGames', 'its good');
					// Update localstorage
					localStorage.setItem('user', JSON.stringify(res));
					this.router.navigate(['/wait']);
				});
			});

		});

	}

}
