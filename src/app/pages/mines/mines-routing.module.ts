import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MinesPage } from './mines.page';

const routes: Routes = [
  {
    path: '',
    component: MinesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MinesPageRoutingModule {}
