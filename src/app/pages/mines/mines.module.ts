import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MinesPageRoutingModule } from './mines-routing.module';

import { MinesPage } from './mines.page';

import { GridComponent } from '../../components/grid/grid.component';

import { CaseComponent } from '../../components/case/case.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MinesPageRoutingModule,
  ],
  declarations: [
    MinesPage,
    GridComponent,
    CaseComponent
  ]
})
export class MinesPageModule {}
