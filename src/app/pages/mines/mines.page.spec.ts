import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MinesPage } from './mines.page';

describe('MinesPage', () => {
  let component: MinesPage;
  let fixture: ComponentFixture<MinesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MinesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
