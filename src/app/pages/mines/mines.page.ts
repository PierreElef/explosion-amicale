import { Component, OnDestroy, OnInit } from '@angular/core';
import { PlayerService } from '../../services/player/player.service';
import { HttpClient } from '@angular/common/http';
import { Player } from '../../models/player';
import { Subscription } from 'rxjs/index';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mines',
  templateUrl: './mines.page.html',
  styleUrls: ['./mines.page.scss'],
})
export class MinesPage implements OnInit, OnDestroy {

  casesValided: string;
  fiveCases = false;
  colorButton = 'danger';
  player: any;

  cdSubcription: Subscription;

  constructor(private playerService: PlayerService, private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
  }

  onCasesValided(res) {
    if (res.substring(1, res.length - 1).split(',').length < 5) {
      this.fiveCases = false;
      this.colorButton = 'danger';
      this.casesValided = res;
    } else if (res.substring(1, res.length - 1).split(',').length === 5) {
      this.fiveCases = true;
      this.colorButton = 'success';
      this.casesValided = res;
    } else {
      this.fiveCases = true;
    }
  }

  onValidPlayer() {
    // Récupération player en localStorage
    this.player = JSON.parse(localStorage.getItem('user'));

    // MAJ player par rapport BDD
    this.playerService.getPlayerById(this.player.id).subscribe((res) => {

      // MAJ mines of player
      res.mines = this.casesValided;
      this.cdSubcription = this.playerService.setPlayerById(this.player.id, res).subscribe();

      localStorage.setItem('user', JSON.stringify(res));

      // Redirect to games-list
      this.router.navigate(['/games-list']);

    });

  }

  ngOnDestroy() {
    if (this.cdSubcription) {
      this.cdSubcription.unsubscribe();
    }
  }

}
