import { Component, OnInit } from '@angular/core';
import { Player } from 'src/app/models/player';
import { PlayerService } from 'src/app/services/player/player.service';
import { GameService } from 'src/app/services/game/game.service';
import { Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';


@Component({
  selector: 'app-wait',
  templateUrl: './wait.page.html',
  styleUrls: ['./wait.page.scss'],
})
export class WaitPage implements OnInit {

  player: Player;

  constructor(private playerService: PlayerService, private gameService: GameService, private router: Router, private socket: Socket) { }

  ngOnInit() {
    this.socket.fromEvent('message').subscribe(data => {
      this.router.navigate(['/game']);
    });
  }

  killGame() {
    // Récupération player en localStorage
    this.player = JSON.parse(localStorage.getItem('user'));
    // MAJ player par rapport BDD
    this.playerService.getPlayerById(this.player.id).subscribe((resPlayer) => {

      const game_id = resPlayer.game_id;
      resPlayer.game_id = null;

      this.playerService.setPlayerById(resPlayer.id, resPlayer).subscribe((resPlayerUpdate) => {

        // Suppression Game et retour sur Game-List
        this.gameService.deleteGameById(game_id).subscribe((res) => {

          // Update localstorage
          localStorage.setItem('user', JSON.stringify(resPlayerUpdate));

          // Refresh games-list page for all clients
          this.socket.emit('refreshListGames', 'its good');

          this.router.navigate(['/games-list']);

        });

      });

    });
  }

}
