import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Game } from '../../models/game';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})

export class GameService {

  // url = 'http://localhost:3000';
  url = 'https://exploamicale.alwaysdata.net';

  constructor(private http: HttpClient) { }

  public getGames(): Observable<Game[]> {
    const url = `${this.url}/games`;
    return this.http.get<Game[]>(url, httpOptions);
  }

  public getGameById(id): Observable<Game> {
    const url = `${this.url}/games/${id}`;
    return this.http.get<Game>(url, httpOptions);
  }

  public createGame(){
    const url = `${this.url}/games`;
    return this.http.post<Game>(url, httpOptions);
  }

  public setGameById(id, game: Game): Observable<Game> {
    const url = `${this.url}/games/${id}`;
    return this.http.put<Game>(url, game, httpOptions);
  }

  public deleteGameById(game: Game | number): Observable<Game>{
    const id = typeof game === 'number' ? game : game.id;
    const url = `${this.url}/games/${id}`;
    return this.http.delete<Game>(url, httpOptions);
  }

  public findGame(): Observable<Game[]> {
    const url = `${this.url}/games/find/game`;
    return this.http.get<Game[]>(url, httpOptions);
  }

}
