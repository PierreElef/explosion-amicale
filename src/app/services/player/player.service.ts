import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Player } from '../../models/player';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  // url = 'http://localhost:3000';
  url = 'https://exploamicale.alwaysdata.net';

  constructor(private http: HttpClient) { }

  public getPlayers(): Observable<Player[]> {
    const url = `${this.url}/players`;
    return this.http.get<Player[]>(url, httpOptions);
  }
  public getPlayerById(id): Observable<Player> {
    const url = `${this.url}/players/${id}`;
    return this.http.get<Player>(url, httpOptions);
  }
  public createPlayer(player: Player): Observable<Player> {
    const url = `${this.url}/players`;
    return this.http.post<Player>(url, player, httpOptions);
  }
  public setPlayerById(id, player: Player): Observable<Player> {
    const url = `${this.url}/players/${id}`;
    return this.http.put<Player>(url, player, httpOptions);
  }
  public deletePlayerById(player: Player | number): Observable<Player>{
    const id = typeof player === 'number' ? player : player.id;
    const url = `${this.url}/players/${id}`;
    return this.http.delete<Player>(url, httpOptions);
  }
}
